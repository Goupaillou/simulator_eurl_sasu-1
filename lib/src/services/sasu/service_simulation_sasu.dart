part of simulator_eurl_sasu;


ServiceSimulationSasu serviceSimulationSasu = new ServiceSimulationSasu();

class ServiceSimulationSasu {

  Logger logger = new Logger('simulator.ServiceSimulationSas');

  Map<int, double> findNetsByPercentage(double turnover, double runningCost) {

    Map<int, double> netByPercentage = new Map();

    for (var i = 0; i <= 100; i++) {
      SimulationSasu simulationSas = computeSimulationBySalaryPercentage(turnover, runningCost, i * 1.0);
      netByPercentage[i] = simulationSas.net;
    }
    return netByPercentage;
  }

  SimulationSasu findBestSimulation(double turnover, double runningCost) {

    SimulationSasu bestSimulation = computeSimulationBySalaryPercentage(turnover, runningCost, 0.00);
    for (var i = 1; i <= 100; i++) {
      SimulationSasu simulationSas = computeSimulationBySalaryPercentage(turnover, runningCost, i * 1.0);
      if(simulationSas.net > bestSimulation.net) {
        bestSimulation = simulationSas;
      }
    }
    return bestSimulation;
  }

  SimulationSasu computeSimulationBySalaryPercentage(double turnover, double runningCost, double salaryPercentage) {

    if(salaryPercentage < 0) {
      salaryPercentage = 0.00;
    }

    if(salaryPercentage > 100) {
      salaryPercentage = 100.00;
    }

    double base = turnover - runningCost;
    double extraGrossTarget = (salaryPercentage * base) / 100;

    double gross = serviceSalary.findGrossFromExtraGross(turnover, runningCost, extraGrossTarget);

    return serviceSimulationSasu.computeSimulation(turnover, runningCost, gross);
  }

  SimulationSasu computeSimulation(double turnover, double runningCost, double gross) {

    Salary salary = serviceSalary.computeSalary(gross);

    double profit = turnover - runningCost - salary.extraGross;

    if(profit < 0) {
      throw 'Profit cannot be negative';
    }

    double taxIs = serviceIrIs.computeIs(profit);
    Dividend dividend = serviceDividend.computeDividend(profit - taxIs);
    double ir = serviceIrIs.computeIr(salary.netTaxable);

    double net = salary.net + dividend.net - ir;

    return new SimulationSasu(turnover, runningCost, salary, taxIs, dividend, ir, net);
  }
}

