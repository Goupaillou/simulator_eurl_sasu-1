part of simulator_eurl_sasu;


ServiceSimulationEurl serviceSimulationEurl = new ServiceSimulationEurl();

class ServiceSimulationEurl {

  Logger logger = new Logger('simulator.ServiceSimulationEurl');

  Map<int, double> findNetsByPercentage(double capital, double turnover, double runningCost) {

    Map<int, double> netByPercentage = new Map();

    for (var i = 0; i <= 100; i++) {
      SimulationEurl simulationEurl = computeSimulationPercentage(capital, turnover, runningCost, i * 1.0);
      netByPercentage[i] = simulationEurl.net;
    }
    return netByPercentage;
  }

  SimulationEurl findBestSimulation(double capital, double turnover, double runningCost) {

    SimulationEurl bestSimulation = computeSimulationPercentage(capital, turnover, runningCost, 0.00);
    for (var i = 1; i <= 100; i++) {
      SimulationEurl simulationEurl = computeSimulationPercentage(capital, turnover, runningCost, i * 1.0);
      if(simulationEurl != null && simulationEurl.net > bestSimulation.net) {
        bestSimulation = simulationEurl;
      }
    }
    return bestSimulation;
  }

  SimulationEurl computeSimulationPercentage(double capital, double turnover, double runningCost, double remunerationPercentage) {

    if(remunerationPercentage < 0) {
      remunerationPercentage = 0.00;
    }

    if(remunerationPercentage > 100) {
      remunerationPercentage = 100.00;
    }

    double maxRemuneration = turnover - runningCost;
    return serviceUtil.findByDichotomy(100, 0.00, maxRemuneration, remunerationPercentage, (newLimit) => computeSimulationEmpiric(capital, turnover, runningCost, newLimit), (_) => true, (computed) => (computed.remuneration.gross / maxRemuneration) * 100);
  }

  SimulationEurl computeSimulationGross(double capital, double turnover, double runningCost, double remunerationGross) {

    return serviceUtil.findByDichotomy(100, 0.00, remunerationGross, remunerationGross, (newLimit) => computeSimulationEmpiric(capital, turnover, runningCost, newLimit), (_) => true, (computed) => computed.remuneration.gross, proportional: true);
  }

  SimulationEurl computeSimulationEmpiric(double capital, double turnover, double runningCost, double remuneration) {

    double maxDividend = turnover - runningCost - remuneration;
    return serviceUtil.findByDichotomy(100, 0.00, maxDividend, 0.00, (newLimit) => computeSimulation(capital, turnover, runningCost, remuneration, newLimit), (computed) => (computed != null && computed.treasury > 0.00), (computed) => computed.treasury, proportional: false);
  }

  SimulationEurl computeSimulation(double capital, double turnover, double runningCost, double remuneration, double dividendGross) {

    Remuneration remunerationEurl = serviceRemuneration.computeRemuneration(remuneration, remuneration + dividendGross);

    double profit = turnover - runningCost - remunerationEurl.gross;

    double taxIs = serviceIrIs.computeIs(profit);
    Dividend dividend = serviceDividendEurl.computeDividend(dividendGross, capital);
    double ir = serviceIrIs.computeIr(remunerationEurl.netTaxable);

    double treasury = turnover - runningCost - remunerationEurl.gross - taxIs - dividendGross;

    double net = remunerationEurl.remuneration + dividend.net - ir;

    return new SimulationEurl(turnover, runningCost, remunerationEurl, taxIs, dividend, ir, net, treasury);
  }
}

