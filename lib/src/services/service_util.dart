part of simulator_eurl_sasu;


NumberFormat format = new NumberFormat("###0.00", "en_US");

double roundNumber(double number) {
  return double.parse(format.format(number));
}

ServiceUtil serviceUtil = new ServiceUtil();

class ServiceUtil {

  Logger logger = new Logger('simulator.ServiceUtil');

  dynamic findByDichotomy(int nbIterations, double begin, double end, double target, dynamic compute(newLimit), bool keepComputed(computed), double getValueToCompare(computed), {bool proportional: true}) {

    var lastComputed = null;
    double newLimit = 0.00;
    for (var i = 0; i < nbIterations; i++) {
      newLimit = (begin + end) / 2;

      var computed = compute(newLimit);

      if(computed != null) {
        if(keepComputed(computed)) {
          lastComputed = computed;
        }

        double valueToCompare = getValueToCompare(computed);

        if(valueToCompare == target) {
          return computed;
        }

        if((valueToCompare < target) == proportional) {
          begin = (begin + end) / 2;
        }

        if(valueToCompare > target == proportional) {
          end = (begin + end) / 2;
        }

      } else {
        end = (begin + end) / 2;
      }
    }

    return lastComputed;
  }
}
