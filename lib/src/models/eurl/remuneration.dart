part of simulator_eurl_sasu;

class Remuneration {

  Logger logger = new Logger('simulator.Remuneration');

  double remuneration;
  double calculationBase;
  double charges;
  double gross;
  double netTaxable;

  List<ChargeEurlComputed> chargesComputed = new List();

  Remuneration.computeFromCharges(double remuneration, double calculationBase, List<ChargeEurlComputed> chargesComputed) {
    this.remuneration = remuneration;
    this.calculationBase = calculationBase;
    this.chargesComputed = chargesComputed;

    this.charges = 0.00;
    for(ChargeEurlComputed chargeComputed in this.chargesComputed) {
      this.charges += chargeComputed.chargeComputed;
    }

    this.gross = remuneration + this.charges;

    List<ChargeEurlComputed> chargesComputedNonDeductibles = chargesComputed.where((ChargeEurlComputed chargeEurlComputed) {
      if(chargeEurlComputed.charge is ChargeEurlCsgCrds) {
        ChargeEurlCsgCrds charge = chargeEurlComputed.charge;
        if(charge.isDeductible == false) {
          return true;
        }
      }
      return false;
    }).toList();

    double sumChargesIndeductible = chargesComputedNonDeductibles.map((charge) => charge.chargeComputed).reduce((charge1, charge2) => charge1 + charge2);
    netTaxable = remuneration + sumChargesIndeductible;
  }

  Map toPublicJson() {
    Map map = new Map();
    map["net"] = roundNumber(remuneration);
    map["base"] = roundNumber(calculationBase);
    map["totalCharges"] = roundNumber(charges);
    map["gross"] = roundNumber(gross);
    map["netTaxable"] = roundNumber(netTaxable);

    List chargesDetail = new List();
    for(ChargeEurlComputed charge in chargesComputed) {
      chargesDetail.add(charge.toPublicJson());
    }

    map["charges"] = chargesDetail;

    return map;
  }

  String toString() {
    return """
      \n
      [Remuneration]
      remuneration = $remuneration
      charges = $charges
      gross = $gross
      chargesComputed = $chargesComputed
      """;
  }
}
