part of simulator_eurl_sasu;

ChargesEurl chargesEurl = new ChargesEurl();

class ChargesEurl {
  List<ChargeEurl> charges = [
    new ChargeEurlRate("Maladie maternité (SSI)", 6.50),
    new ChargeEurlThreshold("Indemnités journalières", 0.85, 0.00, 5.00 * pass),
    new ChargeEurlThreshold(
        "Retraite de base TA", 17.75, 0.00, 1.00*pass),
    new ChargeEurlThreshold(
        "Retraite de base TB", 0.60, 1.00*pass, double.infinity),
    new ChargeEurlThreshold("Retraite complémentaire TA", 7.00, 0.00, 37846),
    new ChargeEurlThreshold("Retraite complémentaire TB", 8.00, 37846, 158928),
    new ChargeEurlThreshold("Invalidité Décès", 1.30, 0.00, 1.00*pass),
    new ChargeEurlRate("Allocations familiales", 3.10),
    new ChargeEurlCsgCrds("CSG non déductible et CRDS (URSSAF)", 2.90, false),
    new ChargeEurlCsgCrds("CSG déductible (URSSAF)", 6.80, true),
    new ChargeEurlFormationProfessionnelle(
        "Contribution pour la formation professionnelle (CFP) (URSSAF)")
  ];
}
