part of simulator_eurl_sasu;


class ChargeEurlInvaliditeDeces extends ChargeEurl with InvaliditeDeces {

  ChargeEurlInvaliditeDeces(String name) : super(name);
}

abstract class InvaliditeDeces implements Compute {

  double compute(double base) {
    return 76.00;
  }
}
