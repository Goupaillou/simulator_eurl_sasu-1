part of simulator_eurl_sasu;


class ChargeSasThresholds extends ChargeSas {

  double thresholdFrom;
  double thresholdTo;

  ChargeSasThresholds(String name, double rateEmployee, double rateEmployer, this.thresholdFrom, this.thresholdTo, {toAddToCsgCrdsBase: false, toApplyToEmployeeOnly: false}) : super(name, rateEmployee, rateEmployer, toAddToCsgCrdsBase: toAddToCsgCrdsBase, toApplyToEmployeeOnly: toApplyToEmployeeOnly);

  double compute(double amount, double taux) {

    double amountToProcess = 0.00;

    if(amount >= thresholdFrom) {
      amountToProcess = amount - thresholdFrom;

      if(amount >= thresholdTo) {
        amountToProcess = thresholdTo - thresholdFrom;
      }
    }

    return amountToProcess * taux/100;
  }

  @override
  double computeEmployer(double amount) {
    return compute(amount, rateEmployer);
  }

  @override
  double computeEmployee(double amount) {
    return compute(amount, rateEmployee);
  }
}
