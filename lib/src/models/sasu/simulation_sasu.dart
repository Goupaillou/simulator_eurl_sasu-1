part of simulator_eurl_sasu;

class SimulationSasu {

  Logger logger = new Logger('simulator.SimulationSas');

  double turnover;
  double runningCost;

  Salary salary;
  double taxIs;
  Dividend dividend;

  double taxIr;
  double net;

  SimulationSasu(this.turnover, this.runningCost, this.salary,
      this.taxIs, this.dividend, this.taxIr, this.net);

  String toString() {
    return """
      \n
      [SimulationSas]
      turnover = $turnover
      runningCost = $runningCost
      salary = $salary
      taxIs = $taxIs
      Dividend = $dividend
      taxIr = $taxIr
      net = $net
      yield = ${roundNumber(net / (turnover - runningCost) * 100)}
      """;
  }

  Map toPublicJson() {
    Map map = new Map();
    map["turnover"] = roundNumber(turnover);
    map["runningCost"] = roundNumber(runningCost);
    map["salary"] = salary.toPublicJson();
    map["is"] = roundNumber(taxIs);
    map["dividend"] = dividend.toPublicJson();
    map["taxIr"] = roundNumber(taxIr);
    map["net"] = roundNumber(net);
    map["yield"] = roundNumber(net / (turnover - runningCost) * 100);

    return map;
  }
}
