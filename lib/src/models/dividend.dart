part of simulator_eurl_sasu;

class Dividend {

  Logger logger = new Logger('simulator.Dividend');

  double gross;
  double charges;
  double net;

  Dividend(this.gross, this.charges, this.net);

  Map toPublicJson() {
    Map map = new Map();
    map["gross"] = roundNumber(gross);
    map["net"] = roundNumber(net);
    map["charges"] = roundNumber(charges);
    return map;
  }

  String toString() {
    return """
      \n
      [Dividend]
      gross = $gross
      charges = $charges
      net = $net
      """;
  }
}
